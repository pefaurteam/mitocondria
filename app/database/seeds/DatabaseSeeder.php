<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Mascota;
use App\Vote;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call('MascotaAppSeeder');
        $this->command->info('Mascota app seeds finished.');
    }
}



class MascotaAppSeeder extends Seeder {

    public function run() {

        // clear our database
        DB::table('mascotas')->delete();
        DB::table('votes')->delete();

        // seed our bears table

       $mascotaTest = Mascota::create(array(
            'name'        => 'Dogo',
            'nickname'    => 'Nicolai',
            'image'       => '1.jpg'
        ));

        $voteTest = Vote::create(array(
            'mascota_id'  => '1',
            'ip'          => '127'
        ));

    }
}
