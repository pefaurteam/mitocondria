<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public $timestamps = false;
    public $createat = false;
    protected $fillable = array('mascota_id', 'ip');
}
