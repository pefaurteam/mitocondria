<?php

namespace App\Http\Controllers;

use App\Mascota;
use Illuminate\Http\Request;

class MascotaController extends Controller
{

    protected function showGallery(){
        $mascotas = $posts = Mascota::all();
        return \View::make("home")->with("mascotas", $mascotas);
    }
    protected function create(array $data)
    {
        return Mascota::create(array(
           'name'        => $data['name'],
           'nickname'    => $data['nickname'],
           'image'       => $data['image']
        ));
    }
}
