<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mascota extends Model
{
    public $timestamps = false;
    public $createat = false;
    protected $fillable = array('name', 'nickname', 'image');

}
