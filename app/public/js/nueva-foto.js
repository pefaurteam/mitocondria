var core = {
    init: function(){
        console.log("core.init");

        var basic = $('#main-cropper').croppie({
            viewport: { width: 250, height: 250 },
            boundary: { width: 300, height: 300 },
            showZoomer: false,
            enableOrientation: true,
            url: 'http://lorempixel.com/500/400/'
        });

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#main-cropper').croppie('bind', {
                        url: e.target.result
                    });
                    $('#rotate, #publicar').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.actionUpload input').on('change', function () { readFile(this); });
        $('#rotate').on('click', function(ev) {
            $('#main-cropper').croppie('rotate', parseInt($(this).data('deg')));
        })
        // subir imagen
        $('#publicar').on('click', function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            var image = $('#main-cropper').croppie('get');


            // upload
            var form = $('form');
            var data = new FormData();
            $.each($(':input', form ), function(i, fileds){
                data.append($(fileds).attr('name'), $(fileds).val());
            });
            $.each($('input[type=file]',form )[0].files, function (i, file) {
                data.append(file.name, file);
            });
            $.ajax({
                url: '/api/upload-image',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (c) {
                    //code here if you want to show any success message
                }
            });

        });
    }
}
$(document).ready(core.init);




