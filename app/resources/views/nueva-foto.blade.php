<!doctype html>
  <html lang="{{ app()->getLocale() }}">
    <head>
      <title>Nueva Foto - Mitocondria</title>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
      <link rel="stylesheet" href="{{ URL::asset('css/jquery.bsPhotoGallery.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.1/croppie.min.css" />

    </head>
    <body>
     <div class="container">

       <div class="row">
         <div class="col">
            <div id="modal">

                <button id="rotate" class="vanilla-rotate btn btn-primary hide" data-deg="-90">Rotar 90°</button>
                <div id="main-cropper"></div>
            </div>
         </div>
         <div class="col">
            <form>
                <div class="form-group">
                     <a class="button actionUpload btn btn-primary">
                        <span class="btn btn-primary">Subir Imagen</span>
                        <input name="image" type="file" id="upload" value="Choose Image" accept="image/*">
                    </a>
                </div>
                <div class="form-group">
                 <label for="exampleInputEmail1">Nickname</label>
                 <input name="nickname" type="text" class="form-control" id="inputNick" aria-describedby="emailHelp" placeholder="Ingresar nickname" required="true">
                </div>
                <div class="form-group">
                 <label for="exampleInputEmail1">Nombre Mascota</label>
                 <input name="mascota" type="text" class="form-control" id="inputName" aria-describedby="emailHelp" placeholder="Ingresar nombre" required="true">
                </div>
                <button type="submit" id="publicar" class="btn btn-success hide" >Publicar</button>
            </form>
         </div>
       </div>

      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.2.1.min.js"  crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.1/croppie.min.js"></script>
      <script src="{{ URL::asset('js/nueva-foto.js') }}"></script>

    </body>
  </html>




