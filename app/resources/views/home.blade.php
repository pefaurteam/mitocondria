<!doctype html>
  <html lang="{{ app()->getLocale() }}">
    <head>
      <title>Mitocondria</title>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
      <link rel="stylesheet" href="{{ URL::asset('css/jquery.bsPhotoGallery.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">

    </head>
    <body>
     <div class="container">
            <div class="row head">
                <h3>Galeria Mascotas</h3>
                <button onclick="window.location='/nueva-foto'" type="button" class="btn btn-primary">Subir Foto</button>
            </div>

            <ul class="row first" data-bsp-ul-id="bsp-OBnY" data-bsp-ul-index="0">

                 @foreach($mascotas as $post)
                    <li class="col-lg-3 col-md-4 col-sm-3 col-xs-4 col-xxs-12 bspHasModal" data-bsp-li-index="0">
                        <div class="imgWrapper"><img alt="{{ $post->name }}" src="/uploads/{{ $post->image }}" class="img-responsive"></div>
                        <div class="text">{{ $post->name }} ({{ $post->nickname }})  <span><button></button></span></div>
                    </li>
                 @endforeach
      </ul>

      </div> <!-- /container -->

      <div class="modal fade" id="bsPhotoGalleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
                <div class="modal-body"></div>
          </div>
        </div>
      </div>

      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    </body>
  </html>




